import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

export type UserDocument = HydratedDocument<User>;

@Schema()
export class User {
  @Prop()
  name: string;
  @Prop()
  username: string;
  @Prop({ unique: true })
  email: string;
  // @Prop({ type: Object })
  // address: {
  //   street: string;
  //   suite: string;
  //   city: string;
  //   zipcode: string;
  //   geo: {
  //     lat: string;
  //     lng: string;
  //   };
  // };
  // @Prop()
  // phone: string;
  // @Prop()
  // website: string;
  // @Prop({ type: Object })
  // company: {
  //   name: string;
  //   catchPhrase: string;
  //   bs: string;
  // };
}

export const UserSchema = SchemaFactory.createForClass(User);
// export const UserSchema = SchemaFactory.createForClass(User);
