import { IsNotEmpty, IsString, IsEmail } from 'class-validator';
export class CreateUserDto {
  @IsString()
  @IsNotEmpty()
  readonly name: string;
  @IsString()
  readonly username: string;
  @IsString()
  readonly email: string;
}
export default CreateUserDto;
