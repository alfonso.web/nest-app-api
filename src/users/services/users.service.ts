import { HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
// import { ConfigService } from '@nestjs/config';
import { ConfigType } from '@nestjs/config';
import config from '../../config';
import { Db } from 'mongodb';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { User } from '../entities/user.entity';
import { CreateUserDto } from '../dto/user.dto';
import { faker } from '@faker-js/faker';

// import { ErrorManager } from 'src/utils/error.manager';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel(User.name) private userModel: Model<User>, // @Inject('MONGO') private databaseMongo: Db, // @Inject(config.KEY) private configService: ConfigType<typeof config>, // private configService: ConfigService
  ) {}
  async findAll(): Promise<User[]> {
    return await this.userModel.find().exec();
    // const users = this.databaseMongo.collection('users');
    // return users.find().toArray();
    // return 'sas';
    // NODE_ENV = prod npm run start: dev
    // NODE_ENV = stag npm run start: dev
    // const database = this.configService.get<string>('DATABASE_NAME');
    // const database = this.configService.apiKey;
    // return database;
  }

  async search(q: string) {
    const users = await this.userModel
      .find({
        name: {
          $regex: '.*' + q + '.*',
        },
      })
      .exec();
    if (!users.length) {
      throw new HttpException('No users found.', HttpStatus.BAD_REQUEST);
    }
    return users;
  }

  async create(payload: CreateUserDto) {
    // const complementData = {
    //   address: {
    //     street: faker.address.street(),
    //     suite: faker.address.secondaryAddress(),
    //     city: faker.address.city(),
    //     zipcode: faker.address.zipCode(),
    //     geo: {
    //       lat: faker.address.latitude(10, -10, 5),
    //       lng: faker.address.longitude(10, -10, 5),
    //     },
    //   },
    //   phone: faker.phone.phoneNumber(),
    //   website: faker.internet.url(),
    //   company: {
    //     name: faker.company.companyName(),
    //     catchPhrase: faker.company.catchPhraseNoun(),
    //     bs: faker.company.catchPhraseDescriptor(),
    //   },
    // };

    const newUser = await this.userModel.create(payload);
    return newUser;
    // console.log(newUser);
    // console.log(payload);
    // const newUser = new this.userModel({
    //   name: payload.name,
    //   username: payload.username,
    //   email: payload.email,
    // });
    // return newUser;
    // newUser.name = payload.name;
    // newUser.username = payload.username;
    // newUser.email = payload.email;
    //   name: payload.name,
    //   username: payload.username,
    //   email: payload.email,
    //   complementData,
    // }

    // const newUser = new this.userModel(payload);
    // console.log(newUser.save());
    // return newUser.save();
    // return newUser;
  }
}
