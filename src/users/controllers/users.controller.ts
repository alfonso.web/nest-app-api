import { Body, Controller, Get, Post, Query } from '@nestjs/common';
import { UsersService } from '../services/users.service';
import { User } from '../entities/user.entity';
import { CreateUserDto } from '../dto/user.dto';

@Controller('users')
export class UsersController {
  constructor(private usersService: UsersService) {}

  @Get()
  async findAll(): Promise<User[]> {
    return await this.usersService.findAll();
  }

  @Get('search')
  async search(@Query('q') q: string) {
    return await this.usersService.search(q);
  }

  @Post('create')
  async create(@Body() payload: CreateUserDto) {
    return await this.usersService.create(payload);
  }
}
