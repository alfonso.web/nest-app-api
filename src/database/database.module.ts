import { Module, Global } from '@nestjs/common';
import { MongoClient } from 'mongodb';
import { ConfigType } from '@nestjs/config';
import config from '../config';
import { MongooseModule } from '@nestjs/mongoose';

@Global()
@Module({
  imports: [
    MongooseModule.forRootAsync({
      useFactory: async (configService: ConfigType<typeof config>) => {
        const { connection, user, password, host, port, dbName } = configService.mongo;
        return {
          uri: `${connection}://${host}:${port}`,
          user,
          pass: password,
          dbName,
        };
      },
      inject: [config.KEY],
    }),
  ],
  providers: [
    // {
    //   provide: 'MONGO',
    //   useFactory: async (configService: ConfigType<typeof config>) => {
    //     const { connection, user, password, host, port, dbName } = configService.mongo;
    //     // const uri = '';
    //     const uri = `${connection}://${user}:${password}@${host}:${port}/?authSource=admin&readPreference=primary`;
    //     // mongodb://root:secret@127.0.0.1:27018/?authSource=admin&readPreference=primary&ssl=false&directConnection=true
    //     const client = new MongoClient(uri);
    //     await client.connect();
    //     const database = client.db(dbName);
    //     return database;
    //   },
    //   inject: [config.KEY],
    // },
  ],
  exports: [MongooseModule],
})
export class DatabaseModule { }
