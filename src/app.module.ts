import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import { UsersModule } from './users/users.module';
import { enviroments } from './enviroments';
import config from './config';
import { DatabaseModule } from './database/database.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      // envFilePath: '.env',
      envFilePath: `${process.env.NODE_ENV || 'development'}.env`,
      // envFilePath: enviroments.process.env.NODE_ENV || '.env',
      load: [config],
      isGlobal: true,
    }),
    UsersModule,
    DatabaseModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
